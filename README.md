# louisgallet.online

This is the **master** branch. This is the main page of the site. This branch is still build. 

## Badges : 
[![Netlify Status](https://api.netlify.com/api/v1/badges/f14a1eeb-2e4d-4964-841c-d90acd499232/deploy-status)](https://app.netlify.com/sites/louisgalletbeta/deploys) <a href="https://codeclimate.com/github/louisgallet95/louisgallet.online/maintainability"><img src="https://api.codeclimate.com/v1/badges/910c5ef10f86d43e90c8/maintainability" /></a> 
[![Test Coverage](https://api.codeclimate.com/v1/badges/910c5ef10f86d43e90c8/test_coverage)](https://codeclimate.com/github/louisgallet95/louisgallet.online/test_coverage)


## Information for developers : 

Please do **not publish** this branch unless authorized. Please test the features locally on your computer.

## French & Mobile ?

You can find the French branch of the site here : [https://github.com/louisgallet95/louisgallet.online/tree/French](https://github.com/louisgallet95/louisgallet.online/tree/French)
As well as the mobile branch of the site here : [https://github.com/louisgallet95/louisgallet.online/tree/mobile](https://github.com/louisgallet95/louisgallet.online/tree/mobile)


